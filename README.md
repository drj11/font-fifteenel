# Font fifteenel

Like sixel, but there's Fifteen Pixels, and it's a font.

All glyphs are mapped from the PUA-A from
U+F0000 to U+F7FFF.
There are 4 pixels below the baseline, and 11 above.
The UPEM is set to the equivalent of 12 pixels;
recommended line-height is 125% (1.25 in "design" apps).

## Example

The idea is that pixel graphics can be drawn using a sequence of
1-pixel wide strips:

    hb-view --margin 2 --font-size 96 --unicodes F0FF0,F0AA0,F0550,F0AA0,F0550,F0FF0 fonts/ttf/Fifteenel.ttf


![a crude box with a simple checkerboard pattern](image/example.png)


[Nina Kalinina](https://www.ninakalinina.com/) suggested Palm Pilot icons as a source of
inspiration and offered this:

    hb-view --foreground=000 --background=896  --margin 2 --font-size 96 --unicodes F0000,F0000,F0ff0,F1808,F0948,F1948,F0968,F19a8,F0948,F0ef0,F0500,F0a00,F0c00,F0000,F0000 Fifteenel.ttf

![low-resolution pixel art of a memo pad and pencil; from the Palm OS icon set](image/memo.svg)

A yinyang from a Mah Jongg game for the Palm Pilot:

    hb-view --margin 2 --font-size 96 --unicodes F03C0,F0FF0,F1678,F1678,F27FC,F21FC,F203C,F201C,F1198,F1998,F0C30,F03C0 fonts/ttf/Fifteenel.ttf

![low-resolution pixel art of a yinyang](image/yinyang.svg)


## Bit Mapped

Each glyph has 15 pixels arranged in a vertical 1×15 stack.
Pixels correspond to bits: the least significant bit corresponds
to the lowest pixel.

The lowest pixel is 4 below the baseline;
in terms of the Unicode codepoints U+Fxxxy, the
least-significant hex digit, `y`, are those pixels
below the baseline, the remainder are above.

# END
