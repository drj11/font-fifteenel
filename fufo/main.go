package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

var pixelHeightP = flag.Float64("height", 10, "nominal height of pixel")
var pixelWidthP = flag.Float64("width", 10, "nominal width of pixel")

func main() {
	flag.Parse()

	ps, _ := filepath.Glob("./*.ufo")
	if len(ps) != 1 {
		ps, _ = filepath.Glob("../*.ufo")
		if len(ps) != 1 {
			log.Fatal("Can't find *.ufo directory")
		}
	}
	root := ps[0]

	err := os.MkdirAll(filepath.Join(root, "glyphs"), 0755)
	if err != nil {
		log.Fatal(err)
	}

	glyphs := []*UFOGlyph{}

	// Collect glyphs
	for bits := 0; bits < 0x8000; bits += 1 {
		ug, err := BitsToCompositeGlyph(bits,
			*pixelWidthP, *pixelHeightP)
		if err != nil {
			log.Print(err)
			continue
		}
		glyphs = append(glyphs, ug)
	}

	// internal pixel.
	pixel := Pixel()

	glyphs = append(glyphs, pixel)

	WriteUFO(root, glyphs)
}

func WriteUFO(root string, glyphs []*UFOGlyph) {
	// Write to .glif files (and build map)
	// Ultimately used to make glyphs/contents.plist
	glyph_file_map := map[string]string{}
	for _, ug := range glyphs {
		w, err := os.OpenFile(filepath.Join(root, "glyphs", ug.Filepath()),
			os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
		if err != nil {
			log.Print(err)
			continue
		}
		bs, err := xml.MarshalIndent(ug, "", "  ")
		_, err = w.Write(bs)
		if err != nil {
			log.Print(err)
		}

		w.Close()

		filename, occupied := glyph_file_map[ug.Name]
		if occupied {
			log.Printf("Glyph name [%s] has been duplicated in file %s and %s.\n",
				ug.Name, filename, ug.Filepath())
		} else {
			glyph_file_map[ug.Name] = ug.Filepath()
		}
	}

	w, err := os.OpenFile(filepath.Join(root, "glyphs", "contents.plist"),
		os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		log.Print(err)
	} else {
		defer w.Close()
		MapToPlist(w, glyph_file_map)
	}
}

func BitsToCompositeGlyph(bits int, xscale, yscale float64) (*UFOGlyph, error) {
	// Number of pixels below the baseline.
	ImageBaseline := 4

	advance_width := yscale

	// PUA-A
	unicode := 0xF0000 + bits
	glyph_name := fmt.Sprintf("u%06X", unicode)

	ug := NewUFOGlyph(glyph_name)
	ug.Advance.Width = &advance_width
	ug.Unicode = append(ug.Unicode,
		Unicode{fmt.Sprintf("%04X", unicode)})

	x := 0.0
	for i := 0; i < 15; i += 1 {
		if (bits>>i)&0x1 != 0 {
			// Least significant bit corresponds to
			// least y-coordinate (below the baseline).
			// x,y of bottom left (for positioning
			// component), in FDU coordinates.
			cx := x * xscale
			cy := float64(i-ImageBaseline) * yscale
			ug.Outline.Component =
				append(ug.Outline.Component, NewComponentAt("_pixel", cx, cy))
		}
	}

	return &ug, nil
}

func Pixel() *UFOGlyph {
	ug := NewUFOGlyph("_pixel")
	ug.Outline.Contour = []Contour{
		Contour{
			Point: []Point{
				Point{X: 10, Y: 10, Type: "line"},
				Point{X: 0, Y: 10, Type: "line"},
				Point{X: 0, Y: 0, Type: "line"},
				Point{X: 10, Y: 0, Type: "line"},
			},
		},
	}
	width := 0.0
	ug.Advance.Width = &width

	return &ug
}

func PixelFromGlif(filepath string) (*UFOGlyph, error) {
	r, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	ug := UFOGlyph{}
	bs, err := io.ReadAll(r)
	if err != nil {
		return nil, err
	}
	err = xml.Unmarshal(bs, &ug)
	if err != nil {
		return nil, err
	}

	// By convention, the pixel is named _pixel
	ug.Name = "_pixel"
	// ... and has no unicode
	ug.Unicode = nil

	return &ug, nil
}

func MapToPlist(w io.Writer, cs map[string]string) {
	fmt.Fprintln(w, "<plist version=\"1.0\">")
	fmt.Fprintln(w, "<dict>")
	for k, v := range cs {
		fmt.Fprintf(w, "<key>%s</key>\n", k)
		fmt.Fprintf(w, "<string>%s</string>\n", v)
	}
	fmt.Fprintln(w, "</dict>")
	fmt.Fprintln(w, "</plist>")
}

type GlyphEntry struct {
	Name    string
	Unicode string
}
