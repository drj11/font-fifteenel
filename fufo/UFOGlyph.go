package main

import "regexp"

// https://unifiedfontobject.org/versions/ufo3/glyphs/glif/
type UFOGlyph struct {
	XMLName struct{} `xml:"glyph"`
	Name    string   `xml:"name,attr"`
	Format  string   `xml:"format,attr"`
	Advance struct {
		Width  *float64 `xml:"width,attr"`
		Height *float64 `xml:"height,attr"`
		// For <advance> exactly one of width or height must
		// be given. <advance> does not have "omitempty" so
		// that empty <advance> elements (which are erroneous)
		// would appear in XML output, as a possible aide to
		// debugging.
	} `xml:"advance"`
	Unicode []Unicode `xml:"unicode,omitempty"`
	Outline Outline   `xml:"outline"`
}

type Unicode struct {
	Hex string `xml:"hex,attr"`
}

type Outline struct {
	Component []Component `xml:"component,omitempty"`
	Contour   []Contour   `xml:"contour,omitempty"`
}
type Contour struct {
	Identifier string  `xml:"identifier,omitempty"`
	Point      []Point `xml:"point,omitempty"`
}
type Point struct {
	X          float64 `xml:"x,attr"`
	Y          float64 `xml:"y,attr"`
	Type       string  `xml:"type,attr,omitempty"`
	Smooth     string  `xml:"smooth,attr,omitempty"`
	Name       string  `xml:"name,attr,omitempty"`
	Identifier string  `xml:"identifier,attr,omitempty"`
}

type Component struct {
	Base       string  `xml:"base,attr"`
	XScale     float64 `xml:"xScale,attr"`
	XyScale    float64 `xml:"xyScale,attr",omitempty`
	YxScale    float64 `xml:"yxScale,attr",omitempty`
	YScale     float64 `xml:"yScale,attr"`
	XOffset    float64 `xml:"xOffset,attr"`
	YOffset    float64 `xml:"yOffset,attr"`
	Identifier string  `xml:"identifier,attr,omitempty"`
}

func NewUFOGlyph(name string) UFOGlyph {
	ug := UFOGlyph{Format: "2", Name: name}
	return ug
}

func (u *UFOGlyph) Filepath() string {
	// For full algorithm (not implemented here), see
	// https://unifiedfontobject.org/versions/ufo3/conventions/#common-user-name-to-file-name-algorithm
	name := u.Name
	name = regexp.MustCompile("[[:upper:]]").ReplaceAllString(name, "${0}_")
	return name + ".glif"
}

func NewComponentAt(base string, x, y float64) Component {
	c := Component{
		Base:    base,
		XScale:  1.0,
		YScale:  1.0,
		XOffset: x,
		YOffset: y,
	}
	return c
}
